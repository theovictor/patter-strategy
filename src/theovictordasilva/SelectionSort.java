package theovictordasilva;
public class SelectionSort implements SortStrategy {
    //sorting method "SelectionSort"
    //ASCENDING
    public void Sort(double[] Arr, SortOrder Ord) {
        switch (Ord) {
            case ASCENDING:
                for (int fixed = 0; fixed < Arr.length; fixed++) {
                    int smaller = fixed;
                    for (int i = smaller + 1; i < Arr.length; i++) {
                        if (Arr[i] < Arr[smaller]) {
                            smaller = i;
                        }//end if
                    }//end for
                    if (smaller != fixed) {
                        double t = Arr[fixed];
                        Arr[fixed] = Arr[smaller];
                        Arr[smaller] = t;
                    }//end if
                }//end for
                break;
            case DESCENDING:
                for (int fixed = 0; fixed < Arr.length; fixed++) {
                    int smaller = fixed;
                    for (int i = smaller + 1; i < Arr.length; i++) {
                        if (Arr[i] > Arr[smaller]) {
                            smaller = i;
                        }//end if
                    }//end for
                    if (smaller != fixed) {
                        double t = Arr[fixed];
                        Arr[fixed] = Arr[smaller];
                        Arr[smaller] = t;
                    }//end if
                }//end for
                break;
        }//end switch-case
    }
}