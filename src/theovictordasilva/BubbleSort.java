package theovictordasilva;

public class BubbleSort implements SortStrategy {
    //sorting method "BubbleSort"
    //ASCENDING
    public void Sort(double[] Arr, SortOrder Ord) {
        switch (Ord) {
            case ASCENDING:
                boolean exchange = true;
                double aux1;
                while (exchange) {
                    exchange = false;
                    for (int i = 0; i < Arr.length - 1; i++) {
                        if (Arr[i] > Arr[i + 1]) {
                            aux1 = Arr[i];
                            Arr[i] = Arr[i + 1];
                            Arr[i + 1] = aux1;
                            exchange = true;
                        }//end if
                    }//end for
                }//end while
                break;
            case DESCENDING:
                boolean exchange1 = true;
                double aux2;
                while (exchange1) {
                    exchange1 = false;
                    for (int i = 0; i < Arr.length - 1; i++) {
                        if (Arr[i] < Arr[i + 1]) {
                            aux2 = Arr[i];
                            Arr[i] = Arr[i + 1];
                            Arr[i + 1] = aux2;
                            exchange1 = true;
                        }//end if
                    }//end for
                }//end while
                break;
        }//end switch-case
    }
}