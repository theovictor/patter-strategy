package theovictordasilva;

import java.util.Arrays;

public class SuperArray {
//class variables
    protected int Lenght; //attribute that stores vector size
    private double[] Arr; //attribute that stores the vector with data from the SuperArray class
    private SortStrategy Sort; //attribute that stores vector sorting strategy
    
//Class builder. Assign the parameter to set the vector size
    public SuperArray(int Lenght) {
        this.Lenght = Lenght;
        Arr = new double[this.Lenght];
    }
//Return vector size
    public int getLenght() {
        return Lenght;
    }
    public void setValue(int Index, double Value) {
//Assigns the value in the vector index.
//Only accepts positive and valid index values.
//Otherwise throws an exception of type ArrayIndexOutOfBoundsException.
        if((Index>=this.Lenght)&&(Index<0)){
            throw new ArrayIndexOutOfBoundsException("Index Error!");
        }else{
            this.Arr[Index] = Value;
        }
    }
    public double getValue(int Index){
//Returns the value that is stored at the index position of the vector.
//Only accepts positive and valid index values.
//Otherwise throws an exception of type ArrayIndexOutOfBoundsException.
        if((Index>=this.Lenght)&&(Index<0)){
            throw new ArrayIndexOutOfBoundsException("Index Error!");
        }else{
            return this.Arr[Index];
        }
    }
    public long Sort(SortStrategy Sort, SortOrder Order){
//Function that returns the time (in milliseconds) it takes to sort the class vector.
//Receives as a parameter an ordering strategy and an order (ascending or descending) for ordering.
        this.Sort = Sort;
        long time1 = System.currentTimeMillis();
        this.Sort.Sort(this.Arr, Order);
        long time2 = System.currentTimeMillis();
        return (time2-time1);
    }
    @Override
    public String toString() {
//Function that returns the string with all values ​​of the class vector.
        //return "SuperArray{" + "Lenght=" + Lenght + ", Arr=" + Arr + ", Sort=" + Sort + '}';
        return "SuperArray{"+Arrays.toString(Arr)+'}';
    }   
}